# Tarea 2

## Parte 1

Iniciar Apache (httpd).

* [Docker Hub - Apache](https://hub.docker.com/_/httpd)

Ejecutar:

```
docker run --name httpd-prueba -p 8000:80 -v "http/:/usr/local/apache2/htdocs/" htdocs
```

## Parte 2

Iniciar wordpress y mysql:5.7

* [Docker Hub - MySQL](https://hub.docker.com/_/mysql)
* [Docker Hub - WordPress](https://hub.docker.com/_/wordpress)